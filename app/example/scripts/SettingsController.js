angular
  .module('example')
  .controller('SettingsController', function($scope, supersonic) {
    $scope.navbarTitle = "Settings";

    $scope.openCamera = function()
    {
    	var options = {
		  quality: 50,
		  allowEdit: true,
		  targetWidth: 300,
		  targetHeight: 300,
		  encodingType: "png",
		  saveToPhotoAlbum: true
		};
    	supersonic.media.camera.takePicture(options).then( function(result){
  			// Do something with the image URI
		});
    }

    $scope.openGallery = function()
    {
    	var options = { quality:50, allowEdit:true, targetHeight: 300, targetWidth:300};

    	supersonic.media.camera.getFromPhotoLibrary(options).then(function(result)
    	{
    		alert(result);
    	});
    }

    // $scope.openCamera = function()
    // {
    // 	supersonic.device.media.camera.takePicture().then(function(){});
    // };  
    // $scope.openGallery = function()
    // {
    // 	supersonic.device.media.camera.getFromPhotoLibrary().then(function(){});
    // };
    	
  });
