angular
  .module('example')
  .controller('ListaController', function($scope, supersonic) {
    $scope.navbarTitle = "Lista";
    var lista = this;
    var loading = false;
    $scope.loadData = function()
    {
      if(loading) 
      {
         alert('already loading');
      }
      else
      {
        alert('start');
        loading = true;
        $.get("http://jsonplaceholder.typicode.com/posts", function(result)
        {
          lista.data = result;
          alert('end');
          loading = false;
        });
        
      }
    };

    // $scope.data = supersonic.data.model('http://jsonplaceholder.typicode.com/posts');
    // $scope.loadData = function()
    // {
    // 	$scope.data.findAll().then(function(result)
    // 	{
    // 		alert(result.count);
    // 	});
    // };
  });
